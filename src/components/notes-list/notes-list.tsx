import { ComponentProps, FC, useEffect } from "react";
import styled from "styled-components";
import {
  $actions,
  actions,
  selectors,
  useAppDispatch,
  useAppSelector,
} from "../../store";
import { Button, Loader } from "../ui";
import { Note } from "../ui/note";

export interface NotesListProps extends ComponentProps<"div"> {}

export const NotesList: FC<NotesListProps> = ({ className }) => {
  const { notes, isLoading, currentNote } = useAppSelector(
    selectors.notes.notes
  );
  const dispatch = useAppDispatch();

  useEffect(() => {
    try {
      dispatch($actions.notes.getAllNotes());
    } catch (e) {
      dispatch(actions.error.setModalVisible("Unexpected Error"));
    }
  }, []);

  const handleSelectNote = (id: string) => () => {
    dispatch(actions.notes.selectNote(id));
  };

  const renderNotes = () => {
    if (isLoading) {
      return <Loader width={50} height={50} color={"orange"} />;
    }

    if (!isLoading && notes.length === 0) {
      return <Title>Create your first note!</Title>;
    }

    return notes.map((note) => (
      <Note
        key={note.id}
        title={note.title}
        isActive={note.id === currentNote?.id}
        onClick={handleSelectNote(note.id)}
      />
    ));
  };

  return <Root className={className}>{renderNotes()}</Root>;
};

const Root = styled("div")`
  display: flex;
  flex-direction: column;
  gap: 10px;

  max-height: 100vh;
  overflow-y: scroll;

  ::-webkit-scrollbar {
    width: 5px;
    border-radius: 5px;
  }

  ::-webkit-scrollbar-track {
    background-color: darkgrey;
  }
`;

const Title = styled.span`
  color: orange;
  font-size: 30px;
  font-weight: 700;
`;
