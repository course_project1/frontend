import styled from "styled-components";
import {
  actions,
  selectors,
  useAppDispatch,
  useAppSelector,
} from "../../store";
import { Button } from "../ui";

export const ErrorModal = () => {
  const dispatch = useAppDispatch();

  const { message, isVisible } = useAppSelector(selectors.error.errorModal);

  return (
    <>
      {isVisible && (
        <Root>
          <Modal>
            <Content>
              <Title>{message}</Title>
              <Button onClick={() => dispatch(actions.error.setModalInvisible())}>
                <ButtonText>Оk</ButtonText>
              </Button>
            </Content>
          </Modal>
        </Root>
      )}
    </>
  );
};

const Root = styled.div`
  width: 100%;
  height: 100%;

  background: grey;

  display: flex;
  justify-content: center;
  align-items: center;
`;

const Modal = styled.div`
  width: 50%;
  height: 50%;

  @media (max-width: 800px) {
    width: 90%;
  }

  background: tomato;

  border-radius: 10px;

  padding: 10px;

  display: flex;
  justify-content: center;
  align-items: center;
`;

const Title = styled.span`
  font-size: 15px;
  font-weight: 600;

  color: white;
`;

const ButtonText = styled.span`
  font-size: 20px;
  font-weight: 600;

  color: white;
`;

const Content = styled.div`
  width: 50%;
  height: 50%;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
`;
