import React, { FC } from "react";
import styled from "styled-components";
import { AuthForm } from "../auth-form";

export interface AuthPopUpProps extends Partial<HTMLDivElement> {}

export const AuthPopUp: FC<AuthPopUpProps> = ({ className }) => {
  return (
    <Root className={className}>
      <AuthForm />
    </Root>
  );
};

const Root = styled.div`
  position: absolute;

  width: 100%;
  height: 100%;

  background-color: rgba(1, 1, 1, 0.3);

  display: flex;
  justify-content: center;
  align-items: center;
`;
