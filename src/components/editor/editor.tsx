import { ComponentProps, FC, useEffect, useState } from "react";
import styled from "styled-components";
import {
  $actions,
  actions,
  selectors,
  useAppDispatch,
  useAppSelector,
} from "../../store";
import { Button } from "../ui";
import { Input } from "../ui/input";

interface EditorProps extends ComponentProps<"div"> {}

export const Editor: FC<EditorProps> = ({ className }) => {
  const dispatch = useAppDispatch();

  const { currentNote } = useAppSelector(selectors.notes.notes);

  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");

  useEffect(() => {
    setTitle(currentNote?.title || "");
    setBody(currentNote?.body || "");
  }, [currentNote]);

  const handleSaveNote = async () => {
    if (currentNote) {
      await dispatch(
        $actions.notes.updateNote({
          id: currentNote.id,
          dto: {
            title,
            body,
          },
        })
      ).unwrap();
    } else {
      await dispatch(
        $actions.notes.createNote({
          title,
          body,
        })
      ).unwrap();
    }
  };

  const handleDeleteNote = async () => {
    await dispatch($actions.notes.deleteNote(currentNote?.id || "")).unwrap();
  };

  const handleShowErrorModalIfCrash = async (callback: () => void) => {
    try {
      await callback();
      handleClearEditor();
    } catch (e: any) {
      dispatch(actions.error.setModalVisible(e.message));
    }
  };

  const handleResetFields = () => {
    setTitle("");
    setBody("");
  };

  const handleDeleteNoteClick = async () => {
    await handleShowErrorModalIfCrash(handleDeleteNote);
  };

  const handleClearEditor = () => {
    dispatch(actions.notes.deleteTargetNote());
    handleResetFields();
  };

  return (
    <Root className={className}>
      <TitleWithContent>
        <Title>Note Title</Title>
        <Input value={title} onChange={(e) => setTitle(e.target.value)} />
      </TitleWithContent>
      <TitleWithContent>
        <Title>Note body</Title>
        <NoteBody onChange={(e) => setBody(e.target.value)} value={body} />
      </TitleWithContent>
      <Button onClick={() => handleShowErrorModalIfCrash(handleSaveNote)}>
        <ButtonText>Save</ButtonText>
      </Button>
      <DeleteButton disabled={!currentNote} onClick={handleDeleteNoteClick}>
        <ButtonText>Delete</ButtonText>
      </DeleteButton>
      <Button onClick={handleClearEditor} disabled={!currentNote}>
        <ButtonText>Create new blank for note</ButtonText>
      </Button>
    </Root>
  );
};

const Root = styled("div")`
  display: flex;
  flex-direction: column;
  gap: 30px;
`;

const NoteBody = styled.textarea`
  width: 100%;
  min-height: 300px;

  resize: none;

  border-radius: 10px;

  padding: 10px;

  outline: none;

  background: silver;
  color: white;

  font-size: 25px;
  font-weight: 900;
`;

const Title = styled.h1`
  color: grey;
  font-size: 30px;

  font-weight: 800;
`;

const TitleWithContent = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
`;

const ButtonText = styled.span`
  color: white;

  font-size: 20px;

  font-weight: 800;
`;

const DeleteButton = styled(Button)`
  background-color: tomato;

  :hover {
    background-color: red;
  }

  :disabled {
    background-color: grey;
  }
`;
