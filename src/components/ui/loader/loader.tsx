import { FC } from "react";
import { TailSpin } from "react-loader-spinner";

export interface LoaderProps {
  color?: string;
  width?: number;
  height?: number;
}

export const Loader: FC<LoaderProps> = ({ ...props }) => {
  return <TailSpin color={"white"} width={20} height={20} {...props} />;
};
