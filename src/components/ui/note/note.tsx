import { ComponentProps, FC } from "react";
import styled from "styled-components";
import { NoteIcon } from "../../../icons";

export interface NoteProps extends ComponentProps<"div"> {
  title: string;
  isActive?: boolean;
}

export const Note: FC<NoteProps> = ({
  className,
  onClick,
  title,
  isActive = false,
}) => {
  return (
    <Root className={className} $isActive={isActive}>
      <Title>{title}</Title>
      <IconWrapper onClick={onClick}>
        <NoteIcon width={25} height={30} fill={"white"} />
      </IconWrapper>
    </Root>
  );
};

const Root = styled("div")<{ $isActive?: boolean }>`
  width: 100%;

  padding: 10px;

  background-color: ${(props) => (props.$isActive ? "silver" : "grey")};

  border-radius: 15px;

  display: flex;
  justify-content: space-between;
  align-items: center;

  cursor: pointer;

  transition: background-color 0.6s;

  :hover {
    background-color: silver;
  }
`;

const Title = styled.div`
  font-size: 20px;
  font-weight: 700;

  color: white;
`;

const IconWrapper = styled.div``;
