import React, { ComponentProps, FC } from "react";
import styled from "styled-components";

export interface InputProps extends ComponentProps<"input"> {}

export const Input: FC<InputProps> = ({
  className,
  defaultValue,
  onChange,
  placeholder,
  value,
  type,
}) => {
  return (
    <Root
      className={className}
      defaultValue={defaultValue}
      onChange={onChange}
      placeholder={placeholder}
      value={value}
      type={type}
    />
  );
};

const Root = styled("input")`
  height: 40px;

  border-radius: 5px;

  background: silver;

  width: 100%;

  padding: 0px 10px;

  outline: none;
  border: none;

  color: white;

  font-weight: 800;
  font-size: 20px;

  ::placeholder {
    color: grey;
  }
`;
