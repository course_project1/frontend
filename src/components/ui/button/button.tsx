import React, { ComponentProps, FC } from "react";
import styled from "styled-components";

export interface ButtonProps extends ComponentProps<"button"> {}

export const Button: FC<ButtonProps> = ({
  children,
  className,
  onClick,
  disabled,
}) => {
  return (
    <Root className={className} onClick={onClick} disabled={disabled}>
      {children}
    </Root>
  );
};

const Root = styled("button")`
  width: 100%;

  height: 40px;

  display: flex;
  justify-content: center;
  align-items: center;

  color: white;

  background-color: orange;

  border-radius: 10px;

  border: none;

  transition: background-color 0.4s;

  :hover {
    background-color: darkorange;
  }
  :disabled {
    background-color: grey;
  }

  cursor: pointer;
`;
