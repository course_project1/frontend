import React, { ComponentProps, FC, useState } from "react";
import styled from "styled-components";
import {
  $actions,
  actions,
  selectors,
  useAppDispatch,
  useAppSelector,
} from "../../store";
import { Button, Loader } from "../ui";
import { Input } from "../ui/input";

interface AuthFormProps extends Partial<ComponentProps<"div">> {}

export const AuthForm: FC<AuthFormProps> = ({ className }) => {
  const { isLoading } = useAppSelector(selectors.auth.auth);
  const dispatch = useAppDispatch();

  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [isRegister, setIsRegister] = useState(true);

  const handleTrySignIn = async () => {
    await dispatch($actions.auth.signIn({ username, password })).unwrap();
  };

  const handleTrySignUp = async () => {
    await dispatch($actions.auth.signUp({ username, password })).unwrap();
  };

  const handleTryAuth = async () => {
    try {
      if (isRegister) {
        await handleTrySignUp();
      } else {
        await handleTrySignIn();
      }
    } catch (e: any) {
      dispatch(actions.error.setModalVisible(e.message));
    }
  };

  return (
    <Root className={className}>
      <Container>
        <Toggler>
          <ToggleElement
            $isSelected={isRegister}
            onClick={() => setIsRegister(true)}
          >
            <ToggleText $isSelected={isRegister}>Register</ToggleText>
          </ToggleElement>
          <ToggleElement
            $isSelected={!isRegister}
            onClick={() => setIsRegister(false)}
          >
            <ToggleText $isSelected={!isRegister}>Authorize</ToggleText>
          </ToggleElement>
        </Toggler>
        <Input
          placeholder="Username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
        <Input
          placeholder="Password"
          value={password}
          type="password"
          onChange={(e) => setPassword(e.target.value)}
        />
        <Button disabled={isLoading} onClick={handleTryAuth}>
          {isLoading ? <Loader /> : <Text>Authorize</Text>}
        </Button>
      </Container>
    </Root>
  );
};

const Root = styled("div")`
  width: 50%;
  height: 50%;

  display: flex;

  flex-direction: column;
  justify-content: center;
  align-items: center;

  background: white;

  border-radius: 30px;

  @media (max-width: 800px) {
    width: 90%;
  }
`;

const Container = styled.div`
  width: 50%;

  height: 50%;

  display: flex;

  flex-direction: column;
  justify-content: space-between;
  align-items: center;

  @media (max-width: 800px) {
    width: 70%;
  }
`;

const Text = styled.span`
  color: white;
  font-weight: 700;
`;

const Toggler = styled.div`
  border: 2px solid orange;

  display: flex;

  justify-content: space-between;

  width: 100%;
  border-radius: 5px;

  height: 30px;
`;

const ToggleElement = styled.div<{ $isSelected?: boolean }>`
  width: 50%;

  border-radius: 3px;

  background-color: ${(props) => (props.$isSelected ? "orange" : "white")};

  display: flex;

  justify-content: center;
  align-items: center;

  height: 100%;

  cursor: pointer;

  transform: background-color 1s;
`;

const ToggleText = styled.span<{ $isSelected?: boolean }>`
  color: ${(props) => (props.$isSelected ? "white" : "orange")};

  font-weight: 400;
`;
