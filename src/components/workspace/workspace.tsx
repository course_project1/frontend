import { FC } from "react";
import styled from "styled-components";
import { Editor } from "../editor";
import { NotesList } from "../notes-list";

export const Workspace: FC = () => {
  return (
    <Root>
      <StyledEditor />
      <Divider />
      <StyledNotesList />
    </Root>
  );
};

const Root = styled("div")`
  height: 100%;

  display: flex;
  justify-content: space-between;

  padding: 10px;

  max-height: 100vh;
  flex: 1;
`;

const Divider = styled.div`
  width: 2px;
  height: 100%;

  background-color: grey;
`;

const StyledEditor = styled(Editor)`
  width: 48%;
`;

const StyledNotesList = styled(NotesList)`
  width: 48%;
`;
