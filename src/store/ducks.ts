import * as auth from "./auth";
import * as error from "./error-modal";
import * as notes from "./notes";

export const actions = {
  error: error.actions,
  notes: notes.actions,
};

export const $actions = {
  auth: auth.$actions,
  notes: notes.$actions,
};

export const selectors = {
  auth: auth.selectors,
  error: error.selectors,
  notes: notes.selectors,
};
