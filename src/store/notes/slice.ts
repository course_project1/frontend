import { createReducer, PayloadAction } from "@reduxjs/toolkit";
import { NoteResponeDto } from "../../api";
import {
  isFullfilledAction,
  isPendingAction,
  isRejectedAction,
} from "../types";
import { actions } from "./actions";
import { $actions } from "./thunk";

interface NotesState {
  isLoading: boolean;
  notes: NoteResponeDto[];
  currentNote: NoteResponeDto | null;
}

const initialState: NotesState = {
  isLoading: true,
  notes: [],
  currentNote: null,
};

export const notesReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(actions.deleteTargetNote.type, (state) => {
      state.currentNote = null;
    })
    .addCase(
      actions.selectNote.type,
      (state, { payload }: PayloadAction<string>) => {
        state.currentNote =
          state.notes.find((note) => note.id === payload) || null;
      }
    )
    .addCase(
      $actions.getAllNotes.fulfilled.type,
      (state, { payload }: PayloadAction<NoteResponeDto[]>) => {
        state.notes = payload;
      }
    )
    .addCase(
      $actions.createNote.fulfilled.type,
      (state, { payload }: PayloadAction<NoteResponeDto>) => {
        state.notes.push(payload);
      }
    )
    .addCase(
      $actions.deleteNote.fulfilled.type,
      (state, { payload }: PayloadAction<string>) => {
        state.notes = state.notes.filter((note) => note.id !== payload);
        state.currentNote = null;
      }
    )
    .addCase(
      $actions.updateNote.fulfilled.type,
      (state, { payload }: PayloadAction<NoteResponeDto>) => {
        state.notes = state.notes.map((note) =>
          note.id !== payload.id ? note : payload
        );
      }
    )
    .addMatcher(isPendingAction, (state) => {
      state.isLoading = true;
    })
    .addMatcher(isRejectedAction, (state) => {
      state.isLoading = false;
    })
    .addMatcher(isFullfilledAction, (state) => {
      state.isLoading = false;
    });
});
