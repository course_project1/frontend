import { createAsyncThunk } from "@reduxjs/toolkit";
import { API, CreateNoteDto, DeleteNoteDto, NoteResponeDto } from "../../api";
import { errorHandler } from "../../common/error-handler";
import { NotesTypes, UpdateNote } from "./types";

const getAllNotes = createAsyncThunk<NoteResponeDto[], void>(
  NotesTypes.getAllNotes,
  async () => {
    try {
      return await API.notes.getAllNotes();
    } catch (e) {
      throw errorHandler(e);
    }
  }
);

const createNote = createAsyncThunk<NoteResponeDto, CreateNoteDto>(
  NotesTypes.createNote,
  async (data) => {
    try {
      return await API.notes.createNote(data);
    } catch (e) {
      throw errorHandler(e);
    }
  }
);

const deleteNote = createAsyncThunk<string, DeleteNoteDto>(
  NotesTypes.deleteNote,
  async (data) => {
    try {
      await API.notes.deleteNote(data);
      return data;
    } catch (e) {
      throw errorHandler(e);
    }
  }
);

const updateNote = createAsyncThunk<NoteResponeDto, UpdateNote>(
  NotesTypes.updateNote,
  async (data) => {
    try {
      return await API.notes.updateNote(data.id, data.dto);
    } catch (e) {
      throw errorHandler(e);
    }
  }
);

export const $actions = {
  createNote,
  updateNote,
  deleteNote,
  getAllNotes,
};
