import { createAction } from "@reduxjs/toolkit";
import { NotesTypes } from "./types";

const selectNote = createAction<string>(NotesTypes.selectNote);

const deleteTargetNote = createAction(NotesTypes.deleteNote);

export const actions = {
  selectNote,
  deleteTargetNote,
};
