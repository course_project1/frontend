import { RootState } from "../config";

export const selectors = {
  notes: (state: RootState) => state.notes,
};
