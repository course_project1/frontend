import { UpdateNoteDto } from "../../api";

export enum NotesTypes {
  getAllNotes = "getAllNotes",
  createNote = "createNote",
  deleteNote = "deleteNote",
  updateNote = "updateNote",

  selectNote = "selectNote",
  deleteTargetNote = "deleteTargetNote",
}

export interface UpdateNote {
  id: string;
  dto: UpdateNoteDto;
}
