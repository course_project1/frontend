import { createAsyncThunk } from "@reduxjs/toolkit";
import { API } from "../../api";
import {
  api,
  SignInDto,
  SignInResponseDto,
  SignUpDto,
  SignUpResponseDto,
} from "../../api";
import { errorHandler } from "../../common/error-handler";

const signIn = createAsyncThunk<SignInResponseDto, SignInDto>(
  "signIn",
  async (data) => {
    try {
      return await API.auth.SignIn(data);
    } catch (e) {
      throw errorHandler(e);
    }
  }
);

const signUp = createAsyncThunk<SignUpResponseDto, SignUpDto>(
  "signUp",
  async (data) => {
    try {
      return await API.auth.SignUp(data);
    } catch (e) {
      throw errorHandler(e);
    }
  }
);

export const $actions = {
  signIn,
  signUp,
};
