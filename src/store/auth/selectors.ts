import { RootState } from "../config";

export const selectors = {
  auth: (state: RootState) => state.auth,
};
