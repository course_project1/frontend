import { createReducer, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { SignInResponseDto, SignUpResponseDto } from "../../api";
import {
  isFullfilledAction,
  isPendingAction,
  isRejectedAction,
} from "../types";
import { $actions } from "./thunk";

export interface AuthState {
  isLoading: boolean;
  user?: SignInResponseDto["user"];
  jwtToken?: SignInResponseDto["jwtToken"];
}

const initialState: AuthState = {
  isLoading: false,
};

export const authReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(
      $actions.signUp.fulfilled.type,
      (state, { payload }: PayloadAction<SignUpResponseDto>) => {
        state.jwtToken = payload.jwtToken;
        state.user = payload.user;
      }
    )
    .addCase(
      $actions.signIn.fulfilled.type,
      (state, { payload }: PayloadAction<SignInResponseDto>) => {
        state.jwtToken = payload.jwtToken;
        state.user = payload.user;
      }
    )
    .addMatcher(isPendingAction, (state) => {
      state.isLoading = true;
    })
    .addMatcher(isRejectedAction, (state) => {
      state.isLoading = false;
    })
    .addMatcher(isFullfilledAction, (state) => {
      state.isLoading = false;
    });
});
