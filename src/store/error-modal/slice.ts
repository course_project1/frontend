import { createReducer, PayloadAction } from "@reduxjs/toolkit";
import { actions } from "./actions";

interface ErrorModalState {
  isVisible: boolean;
  message: string | null;
}

const initialState: ErrorModalState = {
  isVisible: false,
  message: null,
};

export const errorModalReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(
      actions.setModalVisible.type,
      (state, { payload }: PayloadAction<string>) => {
        state.isVisible = true;
        state.message = payload;
      }
    )
    .addCase(actions.setModalInvisible.type, (state) => {
      state.isVisible = false;
      state.message = null;
    });
});
