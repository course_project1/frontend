import { createAction } from "@reduxjs/toolkit";

export enum ErrorModalTypes {
  setModalVisible = "setModalVisible",
  setModalInvisible = "setModalInvisible",
}

const setModalVisible = createAction<string>(ErrorModalTypes.setModalVisible);

const setModalInvisible = createAction(ErrorModalTypes.setModalInvisible);

export const actions = {
  setModalInvisible,
  setModalVisible,
};
