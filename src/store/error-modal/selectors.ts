import { RootState } from "../config";

export const selectors = {
  errorModal: (state: RootState) => state.error,
};
