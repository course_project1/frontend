export const errorHandler = (e: any) => {
  const message = e.response.data.message;

  if (typeof message === "string") {
    return new Error(message);
  }

  if (Array.isArray(message)) {
    return new Error(message.join(", "));
  }

  return e;
};
