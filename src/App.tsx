import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { DefaultLayout } from "./layouts";
import { Workspace } from "./components/workspace";
import { selectors, useAppSelector } from "./store";
import { AuthPopUp } from "./components/auth-pop-up";
import { ErrorModal } from "./components";

function App() {
  const { jwtToken } = useAppSelector(selectors.auth.auth);
  return (
    <>
      <ErrorModal />
      {jwtToken ? (
        <DefaultLayout>
          <Workspace />
        </DefaultLayout>
      ) : (
        <AuthPopUp />
      )}
    </>
  );
}

export default App;
