import { ComponentProps, FC } from "react";
import styled from "styled-components";
import { NoteIcon } from "../icons/note/note";

export interface DefaultLayoutProps extends ComponentProps<"div"> {}

export const DefaultLayout: FC<DefaultLayoutProps> = ({
  children,
  className,
}) => {
  return (
    <Root className={className}>
      <Header>
          <Title>Notes</Title>
        <NoteIcon width={25} height={25} fill={"white"} />
      </Header>
      <Content>{children}</Content>
    </Root>
  );
};

const Root = styled("div")`
  width: 100%;
  height: 100%;
`;

const Header = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  gap: 20px;

  height: 10%;
  max-height: 80px;

  background-color: grey;
`;

const Content = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  height: 90%;
`;

const Title = styled.h1`
  color: white;
  font-weight: 800;
  font-size: 30px;
`;
