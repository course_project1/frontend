import { httpClient } from "../config";
import { paths } from "../constants";
import {
  SignInDto,
  SignInResponseDto,
  SignUpDto,
  SignUpResponseDto,
} from "../types";

export const SignIn = async (data: SignInDto): Promise<SignInResponseDto> => {
  const response = await httpClient.post(paths.signIn, JSON.stringify(data));
  return response.data;
};

export const SignUp = async (data: SignUpDto): Promise<SignUpResponseDto> => {
  const response = await httpClient.post(paths.signUp, JSON.stringify(data));
  return response.data;
};

export const api = {
  SignIn,
  SignUp,
};
