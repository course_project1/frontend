import * as api from "../api";

export type SignInDto = api.components["schemas"]["SignInDto"];

export type SignUpDto = api.components["schemas"]["SignUpDto"];

export type SignInResponseDto = api.components["schemas"]["SignInResponseDto"];

export type SignUpResponseDto = api.components["schemas"]["SignUpResponseDto"];

export type CreateNoteDto = api.components["schemas"]["CreateNoteDto"];
export type UpdateNoteDto = api.components["schemas"]["UpdateNoteDto"];

export type NoteResponeDto = {
  id: string;
  title: string;
  body: string;
  userId: string;
};

export type GetNoteDto = string;

export type DeleteNoteDto = string;