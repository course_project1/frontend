import * as auth from "./auth";
import * as notes from "./notes";

export * from "./auth";
export * from "./types";

export const API = {
  auth: auth.api,
  notes: notes.api,
};
