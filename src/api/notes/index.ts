import { httpClient } from "../config";
import { paths } from "../constants";
import {
  CreateNoteDto,
  DeleteNoteDto,
  NoteResponeDto,
  UpdateNoteDto,
} from "../types";

export const getAllNotes = async (): Promise<NoteResponeDto[]> => {
  const response = await httpClient.get(paths.notes);
  return await response.data;
};

export const createNote = async (
  data: CreateNoteDto
): Promise<NoteResponeDto> => {
  const response = await httpClient.post(paths.notes, JSON.stringify(data));
  return await response.data;
};

export const deleteNote = async (
  data: DeleteNoteDto
): Promise<NoteResponeDto> => {
  const response = await httpClient.delete(paths.notesId + data);
  return await response.data;
};

export const updateNote = async (
  id: string,
  data: UpdateNoteDto
): Promise<NoteResponeDto> => {
  const response = await httpClient.patch(
    paths.notesId + id,
    JSON.stringify(data)
  );
  return await response.data;
};

export const api = {
  deleteNote,
  createNote,
  updateNote,
  getAllNotes,
};
