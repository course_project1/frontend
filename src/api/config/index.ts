import axios, { AxiosRequestConfig } from "axios";
import { store } from "../../store";
import { API_CONSTANTS } from "../constants";

const config: AxiosRequestConfig = {
  baseURL: API_CONSTANTS.BASE_API_PATH,
  headers: {
    "Content-Type": "application/json",
  },
};

export const httpClient = axios.create(config);

httpClient.interceptors.request.use((config) => {
  const token = store.getState().auth.jwtToken;
  if (token) {
    config.headers!.Authorization = `Bearer ${token}`;
  }
  return config;
});

