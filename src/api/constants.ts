export const API_CONSTANTS = {
  BASE_API_PATH: "https://courseprojectbackend.herokuapp.com",
};

export enum paths {
  signIn = "/auth/signIn",
  signUp = "/auth/signUp",
  notes = "/notes",
  notesId = "/notes/",
}
